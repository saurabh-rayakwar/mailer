package thirdparty

import (
	"fmt"

	"github.com/spf13/viper"
)

type SparkPostConfig struct {
	SPARKPOST_API_KEY  string
	SPARKPOST_BASE_URL string
	TRANSMISSION_URL   string
	FROM_EMAIL         string
	USE_SANDBOX        bool
	FEEDBACK_LINK      string
}

//SendTemplateMail handles sending mail through sparkpost
func SendTemplateMail(to string, toData map[string]string, attachment ...string) {

	spC := &SparkPostConfig{
		viper.GetString("SPARKPOST_API_KEY"),
		viper.GetString("SPARKPOST_BASE_URL"),
		viper.GetString("TRANSMISSION_URL"),
		viper.GetString("FROM_EMAIL"),
		viper.GetBool("USE_SANDBOX"),
		viper.GetString("FEEDBACK_LINK"),
	}

	switch toData["templateId"] {
	case "cs-no-response-email":
		sendNoResponseEmail(*spC, to, toData)
	case "cs-feedback-email":
		sendFeedbackEmail(*spC, to, toData)
	}

}

func sendNoResponseEmail(spC SparkPostConfig, to string, toData map[string]string) {
	requestData := ``

	if spC.USE_SANDBOX {
		requestData = `{
			"options":{
				"sandbox":true
			},
			"content": {
			  "template_id": "cs-no-response-email",
			  "use_draft_template": true
			},
			"substitution_data": {
				"Name": "` + toData["firstName"] + ` ` + toData["lastName"] + `",
				"Time": "` + toData["sessionStartTime"] + `",
				"Email": "version@tech.edureka.in"
			  },
			"recipients": [
			  {
				"address": {
				  "email": "` + to + `",
				  "name": "` + toData["firstName"] + ` ` + toData["lastName"] + `"
				}
			  }
			]
		  }`
	} else {
		requestData = `{
			"content": {
			  "template_id": "cs-no-response-email",
			  "use_draft_template": true
			},
			"substitution_data": {
				"Name": "` + toData["firstName"] + ` ` + toData["lastName"] + `",
				"Time": "` + toData["sessionStartTime"] + `",
				"Email": "version@tech.edureka.in"
			  },
			"recipients": [
			  {
				"address": {
				  "email": "` + to + `",
				  "name": "` + toData["firstName"] + ` ` + toData["lastName"] + `"
				}
			  }
			]
		  }`
	}

	fmt.Print(requestData)

	requestHeaders := map[string]string{
		"Content-Type":  "application/json",
		"Authorization": spC.SPARKPOST_API_KEY,
	}

	rc := NewRequestUtility(spC.SPARKPOST_BASE_URL,
		"POST",
		requestHeaders,
	)
	rc.makeJSONRequest(spC.TRANSMISSION_URL, requestData)
}

func sendFeedbackEmail(spC SparkPostConfig, to string, toData map[string]string) {
	requestData := ``

	if spC.USE_SANDBOX {
		requestData = `{
			"options":{
				"sandbox":true
			},
			"content": {
			  "template_id": "cs-feedback-email",
			  "use_draft_template": true
			},
			"substitution_data": {
				"Learner_Name": "` + toData["learnerName"] + `",
				"Counsellor_Name": "` + toData["counsellorName"] + `",
				"Feedback_Link":"` + spC.FEEDBACK_LINK + `"
			  },
			"recipients": [
			  {
				"address": {
				  "email": "` + to + `",
				  "name": "` + toData["firstName"] + ` ` + toData["lastName"] + `"
				}
			  }
			]
		  }`
	} else {
		requestData = `{
			"content": {
			  "template_id": "cs-feedback-email",
			  "use_draft_template": true
			},
			"substitution_data": {
				"Learner_Name": "` + toData["learnerName"] + `",
				"Counsellor_Name": "` + toData["counsellorName"] + `",
				"Feedback_Link":"` + spC.FEEDBACK_LINK + `"
			  },
			"recipients": [
			  {
				"address": {
				  "email": "` + to + `",
				  "name": "` + toData["firstName"] + ` ` + toData["lastName"] + `"
				}
			  }
			]
		  }`
	}

	requestHeaders := map[string]string{
		"Content-Type":  "application/json",
		"Authorization": spC.SPARKPOST_API_KEY,
	}

	rc := NewRequestUtility(spC.SPARKPOST_BASE_URL,
		"POST",
		requestHeaders,
	)
	rc.makeJSONRequest(spC.TRANSMISSION_URL, requestData)
}

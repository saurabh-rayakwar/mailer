package main

import (
	"fmt"
	"log"

	sp "../../thirdparty/sparkpost.go"
	"github.com/spf13/viper"
)

func init() {
	viper.SetConfigFile(`../../configs/development.json`)
	err := viper.ReadInConfig()

	if err != nil {
		panic(err)
	}

	if viper.GetBool(`debug`) {
		fmt.Println("Service RUN on DEBUG mode")
	}

}

func main() {
	
}

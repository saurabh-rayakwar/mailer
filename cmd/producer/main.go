package main

import (
	"fmt"

	kingpin "gopkg.in/alecthomas/kingpin.v2"

	"github.com/Shopify/sarama"
)

var (
	brokerList = kingpin.Flag("brokerList", "List of brokers to connect").Default("192.168.1.51:9092").Strings()
	topic      = kingpin.Flag("topic", "Topic name").Default("important").String()
	maxRetry   = kingpin.Flag("maxRetry", "Retry limit").Default("5").Int()
)

func main() {
	kingpin.Parse()
	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Retry.Max = *maxRetry
	config.Producer.Return.Successes = true
	producer, err := sarama.NewSyncProducer(*brokerList, config)
	if err != nil {
		panic(err)
	}
	defer func() {
		if err := producer.Close(); err != nil {
			panic(err)
		}
	}()

	// var jsonByteArray = []byte(`
	// 	{"email": "saurabh.rayakwar@edureka.co",
	// 	"firstName": "Saurabh",
	// 	"lastName":"Rayakwar",
	// 	"sessionStartTime":"08:00 AM"
	// 	,"templateId":"cs-no-response-email"}
	// `)

	var jsonByteArray = []byte(`
		{"email": "saurabh.rayakwar@edureka.co",
		"learnerName": "Learner Name",
		"counsellorName":"Counsellor Name",
		"templateId":"cs-feedback-email"}
	`)

	msg := &sarama.ProducerMessage{
		Topic: *topic,
		Value: sarama.ByteEncoder(jsonByteArray),
	}
	partition, offset, err := producer.SendMessage(msg)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Message is stored in topic(%s)/partition(%d)/offset(%d)\n", *topic, partition, offset)
}
